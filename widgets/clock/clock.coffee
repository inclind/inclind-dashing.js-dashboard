class Dashing.Clock extends Dashing.Widget

  ready: ->
    setInterval(@startTime, 500)

  startTime: =>
    today = new Date()

    h = today.getHours()
    formattedHours = if h == 0 or h == 12 then "12" else "#{h % 12}"
    m = today.getMinutes()
    formattedMinutes = if m < 10 then "0#{m}" else "#{m}"
    s = today.getSeconds()
    m = @formatTime(m)
    s = @formatTime(s)
    @set('time', formattedHours + ":" + formattedMinutes + ":" + s)
    @set('date', today.toDateString())

  formatTime: (i) ->
    if i < 10 then "0" + i else i