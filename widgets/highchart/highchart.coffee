class Dashing.Highchart extends Dashing.Widget

  createChart: (series,x_axis,y_axis) ->
    container = $(@node).find('.highchart-container')
    if $(container)[0]
      @chart = new Highcharts.Chart
        chart:
          style: {
            color: "black"
          }
          renderTo: $(container)[0]
          type: "bar"
          backgroundColor: 'none'
          borderRadius: 0
          plotBackgroundColor: '#fefce9'
        series: series
        xAxis: {
          categories: x_axis
        }
        yAxis: {
          title: {
            style: {
              color: "#231f20"
            }
            text: 'Hours Worked'
          }
        }
        title: {
          text: 'Productivity'
          fontSize: '16px'
          fontFamily: 'Verdana, sans-serif'
          useHTML: true
        }
        plotOptions: {
          series: {
            stacking: 'normal'
            color: '#ec4024'
          }
          line: {
            dataLabels: {
              enabled: false
            }
          }
        }
        exporting: {
          enabled: false
        }
        legend: {
          enabled: false
        }
        labels: {
          rotation: -45,
          align: 'right',
          style: {
            color: 'white',
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
            }
          }

  onData: (data) ->
    @createChart(data.series,data.x_axis,data.y_axis)