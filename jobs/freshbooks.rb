require 'ruby-freshbooks'
require 'active_support/core_ext/date/calculations.rb'
require 'active_support/core_ext/date_time/calculations.rb'

DATE_FORMAT = '%F %T'
PER_PAGE = 100
ENDPOINT = 'inclind.freshbooks.com'
TOKEN = 'f0e872e782251f87d5a7491699f2877c'

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '15m', :first_in => 0 do |job|
  freshbookconnection = FreshBooks::Client.new(ENDPOINT, TOKEN)


 / Data aquisition /
  getstaff = freshbookconnection.staff.list
  getprojects = freshbookconnection.project.list()
  getestimates = freshbookconnection.estimate.list()
  getclients = freshbookconnection.client.list()
  gettasks = freshbookconnection.task.list()
  gettimeentries = freshbookconnection.time_entry.list(
      :per_page => PER_PAGE,
      :date_from => DateTime.now.utc.at_beginning_of_week.strftime(DATE_FORMAT),
      :date_to => DateTime.now.utc.strftime(DATE_FORMAT))


  / Data transformation /
  projects = getprojects["projects"]["project"]
  time_entries = gettimeentries["time_entries"]["time_entry"]
  staff = getstaff["staff_members"]["member"]
  clients = getclients["clients"]["client"]
  tasks = gettasks["tasks"]["task"]

  /Metrics for widgets/
  staff_total_hours = []
  weekly_hours = 0
  connor_logged_hours = 0
  dalton_logged_hours = 0
  nick_logged_hours = 0
  bryan_logged_hours = 0
  kevin_logged_hours = 0
  veronica_logged_hours = 0
  mark_logged_hours = 0
  nancy_logged_hours = 0

  clients.each do |i|
    client_id = i.assoc("client_id").last
    organization = i.assoc("organization").last
    first_name = i.assoc("first_name").last
    last_name = i.assoc("last_name").last
    email = i.assoc("email").last
    home_phone = i.assoc("home_phone").last
    primary_street = i.assoc("p_street1").last
    primary_city = i.assoc("p_city").last
    primary_state = i.assoc("p_state").last
    primary_zipcode = i.assoc("p_code").last
    account_url = i["links"].assoc("statement").last
  end

  staff.each do |t|
    first = t.assoc("first_name").last
    last = t.assoc("last_name").last
    id = t.assoc("staff_id").last
    staff_hours = 0


    time_entries.each do |e|
      if e.assoc("staff_id").last == id
        staff_hours += e.assoc("hours").last.to_f
      end

      case e.assoc("staff_id").last
        when '223'
          connor_logged_hours += e.assoc("hours").last.to_f
        when '224'
          dalton_logged_hours += e.assoc("hours").last.to_f
        when '214'
          veronica_logged_hours += e.assoc("hours").last.to_f
        when '199'
          nick_logged_hours += e.assoc("hours").last.to_f
        when '195'
          mark_logged_hours += e.assoc("hours").last.to_f
        when '6'
          kevin_logged_hours += e.assoc("hours").last.to_f
        when '3'
          nancy_logged_hours += e.assoc("hours").last.to_f
        when '144'
          bryan_logged_hours += e.assoc("hours").last.to_f
        else
          puts 'untracked hours escaping'
      end
    end


    staff_total_hours.push(
        first: first,
        last: last,
        id: id,
        staff_hours: staff_hours
    )


    #test_array.push(staff_member)
    #test_array.push(t.rassoc("daltontyndall").first)
    #name = t[:first_name]
    #last = t[:last_name]
    #staff_id = t[:staff_id]
    #puts t.value? 'Dalton'
    #test_array.push(t.to_a)
    weekly_hours += staff_hours

  end
#weekly_time_entries = r["time_entries"]["time_entry"].is_a?(Hash) ? [r["time_entries"]["time_entry"]] : r["time_entries"]["time_entry"]

# Week so far
#week_so_far_hours = 0

#week_so_far_hours = weekly_time_entries.collect{|e| e["hours"].to_i }.inject(:+)
  send_event('week_so_far_hours', { :value =>  weekly_hours.to_int })
#week_so_far_hours = 0

end
