require 'twitter'


#### Get your twitter keys & secrets:
#### https://dev.twitter.com/docs/auth/tokens-devtwittercom
Twitter.configure do |config|
  config.consumer_key = 'muJynJyepihY3mqh8CD85g'
  config.consumer_secret = 'yLPGnYxIddYj8xyxK63TxTyb4alkVk86l29gtsIc5k'
  config.oauth_token = '15353252-w6j0ZHkD1tAj6j3YYMbppCqcAbrnJJnYlO74jrQFD'
  config.oauth_token_secret = 'yntIzidJUfA3NHkAHQd3CHOoIRwwVTN4Zd0j4INGc'
end

search_term = URI::encode('@inclindinc')

SCHEDULER.every '15m', :first_in => 0 do |job|
  begin
    tweets = Twitter.search("#{search_term}").results

    if tweets
      tweets.map! do |tweet|
        { name: tweet.user.name, body: tweet.text, avatar: tweet.user.profile_image_url_https }
      end
      send_event('twitter_mentions', comments: tweets)
    end
  rescue Twitter::Error
    puts "\e[33mFor the twitter widget to work, you need to put in your twitter API keys in the jobs/twitter.rb file.\e[0m"
  end
end